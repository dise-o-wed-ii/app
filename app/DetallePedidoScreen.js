import React, { Component } from 'react';
import { View, Text, StyleSheet, Button, Modal } from 'react-native';

class DetallePedidoScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
    };
  }

  render() {
    const { navigation, route } = this.props; // Obtiene navigation y route de las props

    const { platosPedidos, categorias } = route.params;

    let total = 0;
    const resumenPedido = Object.keys(platosPedidos).map((plato) => {
      const cantidad = platosPedidos[plato];
      const { precio } = categorias
        .map((categoria) => categoria.platos)
        .flat()
        .find((item) => item.nombre === plato);
      const subtotal = cantidad * precio;
      total += subtotal;
      return { plato, cantidad, precio, subtotal };
    });

    return (
      <View style={styles.container}>
        <Text style={styles.appTitle}>Detalle del Pedido</Text>
        <View style={styles.tableContainer}>
          <View style={styles.tableRow}>
            <Text style={styles.tableHeader}>Plato</Text>
            <Text style={styles.tableHeader}>Cantidad</Text>
            <Text style={styles.tableHeader}>Precio Unitario</Text>
            <Text style={styles.tableHeader}>Subtotal</Text>
          </View>
          {resumenPedido.map((item, index) => (
            <View key={index} style={styles.tableRow}>
              <Text style={styles.tableCell}>{item.plato}</Text>
              <Text style={styles.tableCell}>{item.cantidad}</Text>
              <Text style={styles.tableCell}>${item.precio.toFixed(2)}</Text>
              <Text style={styles.tableCell}>${item.subtotal.toFixed(2)}</Text>
            </View>
          ))}
        </View>
        <Text style={styles.total}>Total: ${total.toFixed(2)}</Text>
        <Button
          title="Confirmar Pedido"
          onPress={() => {
            this.setState({ showModal: true });
          }}
        />
        <Modal
          animationType="slide"
          transparent={true}
          visible={this.state.showModal}
        >
          <View style={styles.modalContainer}>
            <View style={styles.modalContent}>
              <Text style={styles.modalText}>¿Estás seguro de confirmar el pedido?</Text>
              <View style={styles.modalButtons}>
                <Button
                  title="Sí"
                  onPress={() => {
                    this.setState({ showModal: false });
                    navigation.navigate('Menu', { platosPedidos }); // Pasar platosPedidos a la pantalla de menú
                  }}
                />
                <Button
                  title="No"
                  onPress={() => {
                    this.setState({ showModal: false });
                  }}
                />
              </View>
            </View>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5F5F5',
  },
  appTitle: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    padding: 16,
    backgroundColor: '#FF9800',
    color: 'black',
  },
  tableContainer: {
    padding: 16,
    borderWidth: 1,
    borderColor: '#000',
  },
  tableRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  tableHeader: {
    flex: 2,
    fontWeight: 'bold',
  },
  tableCell: {
    flex: 2,
  },
  total: {
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'right',
    paddingRight: 16,
    marginTop: 8,
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalContent: {
    backgroundColor: '#FFF',
    padding: 20,
    borderRadius: 10,
    alignItems: 'center',
  },
  modalText: {
    fontSize: 18,
    marginBottom: 20,
  },
  modalButtons: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
  },
});

export default DetallePedidoScreen;
