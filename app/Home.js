// HeaderButtons.js
import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet, Image } from 'react-native';

const Home = ({ navigation, onCustomButtonPress }) => {
  return (
    <View style={styles.container}>
      {/* Espacio para el logo (puedes colocar tu logo aquí) */}
      <View style={styles.logoContainer}>
        {/* Aquí puedes colocar tu logo */}
        {<Image source={require('../assets/logo3.jpg')} style={styles.logo} />}
      </View>

      {/* Botones centrados uno debajo del otro */}
      <View style={styles.buttonsContainer}>
        <TouchableOpacity onPress={() => navigation.navigate('Menu')} style={[styles.button, { backgroundColor: '#FFA500' }]}>
          <Text style={styles.menuButton}>Menu</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={onCustomButtonPress} style={[styles.button, { backgroundColor: '#008000' }]}>
          <Text style={styles.customButton}>Personalizado</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    alignItems: 'center',
    paddingTop: 80,
  },
  logoContainer: {
    marginBottom: 20, // Ajusta el margen inferior del logo
    alignSelf: 'center', // Centrar la imagen horizontalmente
  },
  buttonsContainer: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  button: {
    padding: 40,
    marginBottom: 10,
    borderRadius: 5,
    width: 150,
    alignItems: 'center',
  },
  menuButton: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'black',
  },
  customButton: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'black',
  },
  logo: {
    width: 200, // Ajusta según el tamaño deseado
    height: 200, // Ajusta según el tamaño deseado
    borderRadius: 50,
    borderCurve: 50,

  },
});

export default Home;
