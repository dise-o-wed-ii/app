import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, Image, StyleSheet, Alert, Button } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import axios from 'axios';
import WeatherBar from './WeatherBar';

const categorias = [
  {
    id: 1,
    nombre: 'Desayuno',
    imagen: require('../assets/desayuno.jpg'),
    platos: [
      { nombre: 'Tostadas', precio: 5.99 },
      { nombre: 'Huevos Revueltos', precio: 7.99 },
      { nombre: 'Batido de frutas', precio: 6.49 },
      { nombre: 'Cereal', precio: 4.99 },
      { nombre: 'Panqueques', precio: 8.99 },
    ],
  },
  {
    id: 2,
    nombre: 'Almuerzo',
    imagen: require('../assets/almuerzo.jpg'),
    platos: [
      { nombre: 'Ensalada Cesar', precio: 9.99 },
      { nombre: 'Sándwich de pavo', precio: 8.49 },
      { nombre: 'Spaghetti a la boloñesa', precio: 11.99 },
      { nombre: 'Pollo asado', precio: 10.99 },
      { nombre: 'Pescado a la parrilla', precio: 12.49 },
    ],
  },
  {
    id: 3,
    nombre: 'Cena',
    imagen: require('../assets/cena.jpg'),
    platos: [
      { nombre: 'Sopa de tomate', precio: 6.99 },
      { nombre: 'Pizza de pepperoni', precio: 9.49 },
      { nombre: 'Tacos de carne', precio: 8.99 },
      { nombre: 'Salmón al horno', precio: 12.99 },
      { nombre: 'Pollo al curry', precio: 10.99 },
    ],
  },
  {
    id: 4,
    nombre: 'Postres',
    imagen: require('../assets/postres.jpg'),
    platos: [
      { nombre: 'Tarta de manzana', precio: 5.99 },
      { nombre: 'Mousse de chocolate', precio: 6.99 },
      { nombre: 'Helado de vainilla', precio: 4.99 },
      { nombre: 'Flan', precio: 4.49 },
      { nombre: 'Pastel de queso', precio: 7.99 },
    ],
  },
  {
    id: 5,
    nombre: 'Exóticos',
    imagen: require('../assets/exoticos.jpg'),
    platos: [
      { nombre: 'Sushi', precio: 12.99 },
      { nombre: 'Curry tailandés', precio: 10.99 },
      { nombre: 'Ceviche peruano', precio: 11.49 },
      { nombre: 'Dim Sum', precio: 9.99 },
      { nombre: 'Tagine marroquí', precio: 13.49 },
    ],
  },
];

class MenuScreen extends Component {
  state = {
    categoriasExpandidas: {},
    categoriaActual: null,
    platosPedidos: {},
    weatherData: null,
  };

  componentDidMount() {
    this.fetchWeatherData();
  }

  fetchWeatherData = async () => {
    try {
      const response = await axios.get(
        'https://api.openweathermap.org/data/2.5/weather?lat=-17.7832115&lon=-63.1821042&appid=f268b6013080d6296705b1c5e0fd6bec'
      );
      this.setState({ weatherData: response.data });
    } catch (error) {
      console.error('Error fetching weather data:', error);
    }
  };

  toggleCategoria = (categoriaId) => {
    this.setState((prevState) => {
      const nuevasCategoriasExpandidas = { ...prevState.categoriasExpandidas };
      const categoriaActual = prevState.categoriaActual;

      if (categoriaActual === categoriaId) {
        nuevasCategoriasExpandidas[categoriaId] = !nuevasCategoriasExpandidas[categoriaId];
        this.setState({
          categoriasExpandidas: nuevasCategoriasExpandidas,
          categoriaActual: null,
        });
      } else {
        nuevasCategoriasExpandidas[categoriaId] = true;
        if (categoriaActual !== null) {
          nuevasCategoriasExpandidas[categoriaActual] = false;
        }
        this.setState({
          categoriasExpandidas: nuevasCategoriasExpandidas,
          categoriaActual: categoriaId,
        });
      }
    });
  };

  navigateToReceta = (plato) => {
    this.props.navigation.navigate(plato);
  };

  handlePedido = (plato) => {
    const { platosPedidos } = this.state;
    const cantidadPedidos = platosPedidos[plato] || 0;

    this.setState({
      platosPedidos: {
        ...platosPedidos,
        [plato]: cantidadPedidos + 1,
      },
    });

  };

  handleEliminarPedido = (plato) => {
    const { platosPedidos } = this.state;
    const cantidadPedidos = platosPedidos[plato] || 0;

    if (cantidadPedidos > 0) {
      this.setState({
        platosPedidos: {
          ...platosPedidos,
          [plato]: cantidadPedidos - 1,
        },
      });
    }
    //Alert.alert('Pedido realizado', `Pedido de ${plato} realizado con éxito.`);

  };

  navigateToDetallePedido = () => {
    const { platosPedidos, categorias } = this.state;
    this.props.navigation.navigate('DetallePedido', { platosPedidos, categorias });
  };

  render() {
    const { platosPedidos } = this.state;

    return (
      <View style={styles.container}>
        {this.state.weatherData && <WeatherBar data={this.state.weatherData} />}
        <Text style={styles.appTitle}>Menu del Día</Text>
        <ScrollView contentContainerStyle={styles.scrollContainer}>
          {categorias.map((categoria) => (
            <View key={categoria.id}>
              <TouchableOpacity
                style={[
                  styles.categoria,
                  {
                    backgroundColor: this.state.categoriasExpandidas[categoria.id]
                      ? '#F5F5F5'
                      : '#FF9800',
                  },
                ]}
                onPress={() => this.toggleCategoria(categoria.id)}
              >
                <Image source={categoria.imagen} style={styles.categoriaImagen} />
                <Text style={styles.categoriaText}>{categoria.nombre}</Text>
              </TouchableOpacity>
              {this.state.categoriasExpandidas[categoria.id] && (
                categoria.platos.map((plato, index) => (
                  <View key={index} style={styles.platoContainer}>
                    <TouchableOpacity
                      style={styles.plato}
                      onPress={() => this.navigateToReceta(plato.nombre)}
                    >
                      <View style={styles.infoPlato}>
                        <Text style={styles.platoText}>{plato.nombre}</Text>
                        <Text style={styles.precioPlato}>${plato.precio.toFixed(2)}</Text>
                      </View>
                    </TouchableOpacity>
                    <View style={styles.iconosContainer}>
                      <TouchableOpacity
                        style={styles.icono}
                        onPress={() => this.handleEliminarPedido(plato.nombre)}
                      >
                        <Icon name="minus" size={16} color="white" />
                      </TouchableOpacity>
                      <Text style={styles.cantidadPlatos}>{platosPedidos[plato.nombre] || 0}</Text>
                      <TouchableOpacity
                        style={styles.icono}
                        onPress={() => this.handlePedido(plato.nombre)}
                      >
                        <Icon name="plus" size={16} color="white" />
                      </TouchableOpacity>
                    </View>
                  </View>
                ))
              )}
            </View>
          ))}
          <Button title="Confirmar Pedido" onPress={() => this.props.navigation.navigate('DetallePedido', { platosPedidos, categorias })} />
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5F5F5',
  },
  appTitle: {
    fontSize: 24,
    fontWeight: 'bold',
    textAlign: 'center',
    padding: 16,
    backgroundColor: '#FF9800',
    color: 'black',
  },
  scrollContainer: {
    padding: 16,
  },
  categoria: {
    backgroundColor: 'white',
    padding: 10,
    marginVertical: 8,
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  categoriaImagen: {
    width: 45,
    height: 45,
    marginRight: 15,
  },
  categoriaText: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  platoContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  plato: {
    backgroundColor: '#FFA371',
    padding: 10,
    margin: 4,
    borderRadius: 5,
    flex: 1,
  },
  infoPlato: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  platoText: {
    fontSize: 16,
  },
  precioPlato: {
    fontSize: 14,
    color: 'black',
  },
  iconosContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icono: {
    backgroundColor: '#FFA371',
    padding: 5,
    borderRadius: 5,
    marginHorizontal: 4,
  },
  cantidadPlatos: {
    fontSize: 16,
  },
});

export default MenuScreen;