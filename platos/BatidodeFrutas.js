import React from 'react';
import { Text, Image, StyleSheet, ScrollView, TouchableOpacity } from 'react-native';

const App = ({ navigation }) => {
  const handleOrder = () => {
    // Aquí puedes agregar la lógica para realizar un pedido.
    // Puedes mostrar una alerta, enviar una solicitud a un servidor, etc.
    // En este caso, se redirige a otra ventana llamada 'OrderScreen'
    navigation.navigate('Menu');
  };
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Text style={styles.title}>Batido de Frutas</Text>
      <Image source={require('../assets/batidodefrutas.jpg')} style={styles.image} />
      <Text style={styles.subtitle}>Ingredientes:</Text>
      <Text style={styles.ingredients}>
        - Frutas mixtas (fresas, plátanos, mangos, etc.){'\n'}
        - Yogur natural{'\n'}
        - Miel o azúcar al gusto{'\n'}
        - Hielo (opcional)
      </Text>
      <Text style={styles.subtitle}>Preparación:</Text>
      <Text style={styles.preparation}>
        1. Lava y pela las frutas, y córtalas en trozos.{'\n'}
        2. Coloca las frutas en una licuadora.{'\n'}
        3. Agrega el yogur natural y la miel o azúcar al gusto.{'\n'}
        4. Si deseas, agrega hielo para obtener una textura más fresca.{'\n'}
        5. Licúa todos los ingredientes hasta obtener una mezcla suave.{'\n'}
        6. Sirve en un vaso grande y disfruta de tu batido de frutas refrescante.
      </Text>
      <TouchableOpacity style={styles.button} onPress={handleOrder}>
        <Text style={styles.buttonText}>Hacer Pedido</Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    padding: 16,
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  image: {
    width: 300,
    height: 200,
    resizeMode: 'cover',
    marginBottom: 10,
    borderRadius: 10,
  },
  subtitle: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  ingredients: {
    fontSize: 16,
    marginBottom: 10,
  },
  button: {
    backgroundColor: 'blue',
    padding: 10,
    borderRadius: 5,
    marginTop: 20,
  },
  buttonText: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default App;
