import React from 'react';
import { View, Text, StyleSheet, Animated } from 'react-native';

const WeatherBar = ({ data }) => {
  const marqueeAnimatedValue = new Animated.Value(0);

  const startAnimation = () => {
    Animated.loop(
      Animated.timing(marqueeAnimatedValue, {
        toValue: 1,
        duration: 10000,
        useNativeDriver: true,
      })
    ).start();
  };

  React.useEffect(() => {
    startAnimation();
  }, []);

  const translateX = marqueeAnimatedValue.interpolate({
    inputRange: [0, 1],
    outputRange: [0, -200], // Adjust the value based on your content width
  });

  return (
    <View style={styles.container}>
      <Animated.View style={{ flexDirection: 'row', transform: [{ translateX }] }}>
        <Text style={styles.text}>{data.name}</Text>
        <Text style={styles.text}>{data.weather[0].description}</Text>
        <Text style={styles.text}>Temp: {`${Math.round(data.main.temp - 273.15)}°C`}</Text>
        <Text style={styles.text}>Max: {`${Math.round(data.main.temp_max - 273.15)}°C`}</Text>
        <Text style={styles.text}>Min: {`${Math.round(data.main.temp_min - 273.15)}°C`}</Text>
        <Text style={styles.text}>{`Humedad: ${data.main.humidity}%`}</Text>
        <Text style={styles.text}>{`Viento: ${Math.round(data.wind.speed * 3.6)} km/hr`}</Text>
      </Animated.View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    overflow: 'hidden',
    backgroundColor: '#3498db',
    padding: 10,
  },
  text: {
    color: '#fff',
    marginRight: 20,
  },
});

export default WeatherBar;
