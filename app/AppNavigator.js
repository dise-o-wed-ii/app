import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import Menu from './Menu';
import EnsaladaCesar from '../platos/EnsalaCesar';
import Tostadas from '../platos/Tostadas';
import HuevosRevueltos from '../platos/HuevosRevueltos';
import BatidodeFrutas from '../platos/BatidodeFrutas';
import Panqueques from '../platos/Panqueques';
import Cereal from '../platos/Cereal';
import DetallePedidoScreen from './DetallePedidoScreen';
import Home from './Home';

const Stack = createNativeStackNavigator();

const AppNavigator = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Inicio" component={Home} />
        <Stack.Screen name="Menu" component={Menu} />
        <Stack.Screen name="Ensalada Cesar" component={EnsaladaCesar} />
        <Stack.Screen name="Tostadas" component={Tostadas} />
        <Stack.Screen name="Huevos Revueltos" component={HuevosRevueltos} />
        <Stack.Screen name="Batido de frutas" component={BatidodeFrutas} />
        <Stack.Screen name="Cereal" component={Cereal} />
        <Stack.Screen name="Panqueques" component={Panqueques} />
        <Stack.Screen name="DetallePedido" component={DetallePedidoScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppNavigator;
